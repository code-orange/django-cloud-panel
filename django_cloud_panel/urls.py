import os

from django.conf import settings
from django.urls import re_path
from django.contrib import admin
from django.urls import include, path
from django.views.generic.base import RedirectView

urlpatterns = [
    path("", RedirectView.as_view(url="/dashboard/", permanent=True)),
    path("dashboard", RedirectView.as_view(url="/dashboard/", permanent=True)),
    path("accounts/", include("django.contrib.auth.urls")),
    path("i18n/", include("django.conf.urls.i18n")),
    re_path(r"", include("user_sessions.urls", "user_sessions")),
    path("admin/", admin.site.urls),
    re_path(r"^watchman/", include("watchman.urls")),
    re_path(r"^robots\.txt", include("robots.urls")),
    re_path(
        r"^security\.txt",
        include("django_mdat_security_txt.django_mdat_security_txt.urls"),
    ),
    re_path(
        r"^\.well-known/security\.txt",
        include("django_mdat_security_txt.django_mdat_security_txt.urls"),
    ),
]

# List modules that should not auto-map path
ignore_modules = []

# Dynamic loading of Cloud Panel urls
for name in os.listdir(settings.PROJECT_ROOT + "/.."):
    if os.path.isdir(name) and name.startswith("django_cloud_panel_"):
        if name in ignore_modules:
            continue

        module_name = name + "." + name
        url_path = name.replace("django_cloud_panel_", "").replace("_", "-")
        urlpatterns.append(path(url_path + "/", include(module_name + ".urls")))

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
